package com.amazonaws.lambda.demo.model;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RequestCarga {
	
	private String serialNumber;
	private String clickType;
	private String batteryVoltage;
	
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getClickType() {
		return clickType;
	}
	public void setClickType(String clickType) {
		this.clickType = clickType;
	}
	public String getBatteryVoltage() {
		return batteryVoltage;
	}
	public void setBatteryVoltage(String batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}
	
	public String toJson() throws JsonProcessingException{
		
		ObjectMapper mapper = new ObjectMapper();
		String json = "";

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("serialNumber", this.getSerialNumber());
		map.put("clickType", this.getClickType());
		map.put("batteryVoltage", this.getBatteryVoltage());

		// convert map to JSON string
		json = mapper.writeValueAsString(map);
		
		return json;
	}
	
	
	 

}
