package com.amazonaws.lambda.demo.operations;

import java.util.LinkedHashMap;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;

public class GetCargaBateria implements RequestHandler<Object, String> {
	
    @Override
    public String handleRequest(Object input, Context context) {

    	// Logger
		LambdaLogger logger = context.getLogger();
		logger.log("-- Evento RECIBIDO  --");		
		
    	
		LinkedHashMap  request = (LinkedHashMap) input;   	
		logger.log(request.toString());
       
        return "OK";
    }

}
